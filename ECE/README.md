# ECE Commands
## Overleaf linking URL:
```https://bitbucket.org/ilikecheese8888_classes/latex/raw/master/ECE/eceCommands.sty```

[Main README page](https://bitbucket.org/ilikecheese8888_classes/latex/src/master/README.md)


## Commands List
### ECE Commands
#### [General Commands](#markdown-header-general-commands)
+	[\ansIn](#markdown-header-ansin)

#### [Unit Commands](#markdown-header-units)
+	[\Amp](#markdown-header-amp)
+	[\voltamp](#markdown-header-voltamp)
+	[\siemen](#markdown-header-siemen)
+	[\VAR, \var](#markdown-header-var)
+	[\rms](#markdown-header-rms)

#### ["Unitspaces" Commands](#markdown-header-unit-spaces)
+	[\current{}](#markdown-header-current)
+	[\voltage{}](#markdown-header-voltage)
+	[\impedance{}](#markdown-header-impedance)
+	[\admittance{}](#markdown-header-admittance)
+	[\capacitance{}](#markdown-header-capacitance)
+	[\inductance{}](#markdown-header-inductance)
+	[\power{}](#markdown-header-power)
+	[\energy{}](#markdown-header-energy)
+	[\averagePower{}](#markdown-header-averagepower)
+	[\reactivePower{}](#markdown-header-reactivepower)
+	[\complexpower{}](#markdown-header-complexpower)
+	[\linearFrequency{}](#markdown-header-linearfrequency)
+	[\angularFrequency{}](#markdown-header-angularfrequency)

#### [Unit Modifier Shortcut Commands](#markdown-header-unit-modifier-shortcuts)
+	[\Tshort](#markdown-header-tshort)
+	[\Gshort](#markdown-header-gshort)
+	[\Mshort](#markdown-header-mshortmega)
+	[\kshort](#markdown-header-kshort)
+	[\mshort](#markdown-header-mshortmicro)
+	[\ushort](#markdown-header-ushort)
+	[\nshort](#markdown-header-nshort)
+	[\pshort](#markdown-header-pshort)
__________
## Commands Overview
###ECE Commands

#### General Commands
__________
##### \ansIn
+	An answer with a note to the side
+	Usage: `\ansIn{<value>}{<variable or other label>}`
+	Results in: `\underline{\underline{<value>}}  \tag{$<variable>$}`	
+	Example:
	+	`12 \si{\Amp}` renders: `12A`
	+	`12\ \si{\Amp}` renders: `12 A`
	
		[Back to List](#markdown-header-unit-commands)


#### Units
__________
##### \Amp
+	A short form of suinit's \ampere
+	Usage: `\si{\ampere}`
+	Results in: `A`	
+	Example:
	+	`12 \si{\Amp}` renders: `12A`
	+	`12\ \si{\Amp}` renders: `12 A`
	
		[Back to List](#markdown-header-unit-commands)
		
###### \voltamp
+	Usage: `\si{\voltamp}`
+	Results in: `VA`
+	Example:
	+	`12 \si{\voltamp}` renders: `12VA`
	+	`12\ \si{\voltamp}` renders: `12 VA`
	
		[Back to List](#markdown-header-unit-commands)
		
##### \siemen
+	Usage: `\si{\seimen}`
+	Results in: `S`
+	Example:
	+	`12 \si{\seimen}` renders: `12S`
	+	`12\ \si{\seimen}` renders: `12 S`
	
		[Back to List](#markdown-header-unit-commands)
		
##### \VAR
+	\var
+	Usage: `\si{\VAR}` or `\si{\var}`
+	Results in: `VAR`
+	Example:
	+	`12 \si{\VAR}` renders: `12VAR`
	+	`12\ \si{\VAR}` renders: `12 VAR`
	+	`12 \si{\var}` renders: `12VAR`
	+	`12\ \si{\var}` renders: `12 VAR`
	
		[Back to List](#markdown-header-unit-commands)
		
##### \rms
+	Usage: `\si{\rms}`
+	Results in: ` (RMS)`
+	Example:
	+	`12 \si{\Amp\rms}` renders: `12A (RMS)`
	+	`12\ \si{\Amp\rms}` renders: `12 A (RMS)`
	
		[Back to List](#markdown-header-unit-commands)
		
		
#### Unit Spaces
__________
##### \current{}
+	Usage: `\current{<value>}` or `\current[<modifier>]{<value>}`
+	Results in: `<value>\ \si{\ampere}` and `<value>\ \si{<modifier>\ampere}` repsectively
+	<modifier> is any unit modifier (i.e \milli, \kilo, etc.) in the siunitx package or otherwise included here
+	Example:
	+	`\current{0.234}` renders: `0.234 A`
	+	`\current[\milli]{234}` renders: `234 mA`
	
		[Back to List](#markdown-header-unitspaces-commands)

##### \voltage{}
+	Usage: `\voltage{<value>}` or `\voltage[<modifier>]{<value>}`
+	Results in: `<value>\ \si{\volt}` and `<value>\ \si{<modifier>\volt}` repsectively
+	<modifier> is any unit modifier (i.e \milli, \kilo, etc.) in the siunitx package or otherwise included here
+	Example:
	+	`\current{2,345}` renders: `2,345 V`
	+	`\current[\kilo]{2.345}` renders: `2.345 kV`
	
		[Back to List](#markdown-header-unitspaces-commands)
	
##### \impedance{}
+	Usage: `\impedance{<value>}` or `\impedance[<modifier>]{<value>}`
+	Results in: `<value>\ \si{\ohm}` and `<value>\ \si{<modifier>\ohm}` repsectively
+	<modifier> is any unit modifier (i.e \milli, \kilo, etc.) in the siunitx package or otherwise included here
+	Example:
	+	`\impedance{234}` renders: `234 Ω`
	+	`\impedance[\mega]{2.34}` renders: `2.34 MΩ`
	
		[Back to List](#markdown-header-unitspaces-commands)
	
##### \admittance{}
+	Usage: `\admittance{<value>}` or `\admittance[<modifier>]{<value>}`
+	Results in: `<value>\ \si{\siemen}` and `<value>\ \si{<modifier>\siemen}` repsectively
+	<modifier> is any unit modifier (i.e \milli, \kilo, etc.) in the siunitx package or otherwise included here
+	Example:
	+	`\admittance{2500}` renders: `2500 S`
	+	`\admittance[\kilo]{1.5}` renders: `1.5 kS`
	
		[Back to List](#markdown-header-unitspaces-commands)
	
##### \capacitance{}
+	Usage: `\capacitance{<value>}` or `\capacitance[<modifier>]{<value>}`
+	Results in: `<value>\ \si{\farad}` and `<value>\ \si{<modifier>\farad}` repsectively
+	<modifier> is any unit modifier (i.e \milli, \kilo, etc.) in the siunitx package or otherwise included here
+	Example:
	+	`\capacitance{0.000234}` renders: `0.000234 F`
	+	`\capacitance[\micro]{234}` renders: `234 µF`
	
		[Back to List](#markdown-header-unitspaces-commands)
		
##### \inductance{}
+	Usage: `\inductance{<value>}` or `\inductance[<modifier>]{<value>}`
+	Results in: `<value>\ \si{\henry}` and `<value>\ \si{<modifier>\henry}` repsectively
+	<modifier> is any unit modifier (i.e \milli, \kilo, etc.) in the siunitx package or otherwise included here
+	Example:
	+	`\inductance{0.234}` renders: `0.234 H`
	+	`\inductance[\milli]{234}` renders: `234 mH`
	
		[Back to List](#markdown-header-unitspaces-commands)
		
##### \power{}
+	Usage: `\power{<value>}` or `\power[<modifier>]{<value>}`
+	Results in: `<value>\ \si{\watt}` and `<value>\ \si{<modifier>\watt}` repsectively
+	<modifier> is any unit modifier (i.e \milli, \kilo, etc.) in the siunitx package or otherwise included here
+	Example:
	+	`\power{2340}` renders: `2340 W`
	+	`\power[\kilo]{2.34}` renders: `2.34 kW`
	
		[Back to List](#markdown-header-unitspaces-commands)
		
##### \energy{}
+	Usage: `\energy{<value>}` or `\energy[<modifier>]{<value>}`
+	Results in: `<value>\ \si{\joule}` and `<value>\ \si{<modifier>\joule}` repsectively
+	<modifier> is any unit modifier (i.e \milli, \kilo, etc.) in the siunitx package or otherwise included here
+	Example:
	+	`\energy{2340}` renders: `2340 J`
	+	`\energy[\kilo]{2.34}` renders: `2.34 kJ`
	
		[Back to List](#markdown-header-unitspaces-commands)
	
##### \averagePower{}
+	Usage: `\averagePower{<value>}` or `\averagePower[<modifier>]{<value>}`
+	Results in: `<value>\ \si{\watt}` and `<value>\ \si{<modifier>\watt}` repsectively
+	<modifier> is any unit modifier (i.e \milli, \kilo, etc.) in the siunitx package or otherwise included here
+	Example:
	+	`\averagePower{2340}` renders: `2340 W`
	+	`\averagePower[\kilo]{2.34}` renders: `2.34 kW`
	
		[Back to List](#markdown-header-unitspaces-commands)
		
##### \reactivePower{}
+	Usage: `\reactivePower{<value>}` or `\reactivePower[<modifier>]{<value>}`
+	Results in: `<value>\ \si{\VAR}` and `<value>\ \si{<modifier>\VAR}` repsectively
+	<modifier> is any unit modifier (i.e \milli, \kilo, etc.) in the siunitx package or otherwise included here
+	Example:
	+	`\reactivePower{2340}` renders: `2340 VAR`
	+	`\reactivePower[\kilo]{2.34}` renders: `2.34 kVAR`
	
		[Back to List](#markdown-header-unitspaces-commands)
		
##### \complexPower{}
+	Usage: `\complexPower{<value>}` or `\complexPower[<modifier>]{<value>}`
+	Results in: `<value>\ \si{\voltamp}` and `<value>\ \si{<modifier>\voltamp}` repsectively
+	<modifier> is any unit modifier (i.e \milli, \kilo, etc.) in the siunitx package or otherwise included here
+	Example:
	+	`\complexPower{2340}` renders: `2340 VA`
	+	`\complexPower[\kilo]{2.34}` renders: `2.34 kVA`
		
		[Back to List](#markdown-header-unitspaces-commands)
		
##### \linearFrequency{}
+	Usage: `\linearFrequency{<value>}` or `\linearFrequency[<modifier>]{<value>}`
+	Results in: `<value>\ \si{\hertz}` and `<value>\ \si{<modifier>\hertz}` repsectively
+	<modifier> is any unit modifier (i.e \milli, \kilo, etc.) in the siunitx package or otherwise included here
+	Example:
	+	`\linearFrequency{2340}` renders: `2340 Hz`
	+	`\linearFrequency[\kilo]{2.34}` renders: `2.34 kHz`
		
		[Back to List](#markdown-header-unitspaces-commands)
		
##### \angularFrequency{}
+	Usage: `\angularFrequency{<value>}` or `\angularFrequency[<modifier>]{<value>}`
+	Results in: `<value>\ \si{\radian\per\sec}` and `<value>\ \si{<modifier>\radian\per\sec}` repsectively
+	<modifier> is any unit modifier (i.e \milli, \kilo, etc.) in the siunitx package or otherwise included here
+	Example:
	+	`\angularFrequency{2340}` renders: `2340 rad/s`
	+	`\angularFrequency[\kilo]{2.34}` renders: `2.34 krad/s`
		
		[Back to List](#markdown-header-unitspaces-commands)
		
#### Unit Modifier Shortcuts
__________

##### \Tshort
+	Usage: `\Tshort`
+	Results in: `\si{\tera}`
+	Example:
	+	`12\Tshort + 3.5\Tshort` renders: `12T + 3.5T`
		
		[Back to List](#markdown-header-unit-modifier-shortcut-commands)

##### \Gshort
+	Usage: `\Gshort`
+	Results in: `\si{\giga}`
+	Example:
	+	`12\Gshort + 3.5\Gshort` renders: `12G + 3.5G`
		
		[Back to List](#markdown-header-unit-modifier-shortcut-commands)

##### \Mshort(Mega)
+	Usage: `\Mshort`
+	Results in: `\si{\mega}`
+	Example:
	+	`12\Mshort + 3.5\Mshort` renders: `12M + 3.5M`
		
		[Back to List](#markdown-header-unit-modifier-shortcut-commands)

##### \kshort
+	Usage: `\kshort`
+	Results in: `\si{\kilo}`
+	Example:
	+	`12\kshort + 3.5\kshort` renders: `12k + 3.5k`
		
		[Back to List](#markdown-header-unit-modifier-shortcut-commands)

##### \mshort(micro)
+	Usage: `\m`
+	Results in: `\si{\milli}`
+	Example:
	+	`12\m + 3.5\m` renders: `12m + 3.5m`
		
		[Back to List](#markdown-header-unit-modifier-shortcut-commands)
		
##### \ushort
+	Usage: `\ushort`
+	Results in: `\si{\micro}`
+	Example:
	+	`12\ushort + 3.5\ushort` renders: `12µ + 3.5µ`
		
		[Back to List](#markdown-header-unit-modifier-shortcut-commands)

##### \nshort
+	Usage: `\nshort`
+	Results in: `\si{\nano}`
+	Example:
	+	`12\nshort + 3.5\nshort` renders: `12n + 3.5n`
		
		[Back to List](#markdown-header-unit-modifier-shortcut-commands)

##### \pshort
+	Usage: `\pshort`
+	Results in: `\si{\pico}`
+	Example:
	+	`12\pshort + 3.5\pshort` renders: `12p + 3.5p`
		
		[Back to List](#markdown-header-unit-modifier-shortcut-commands)