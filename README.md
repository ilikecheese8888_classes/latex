# Latex
## Linking a file to overleaf
Use the following format when uploading to overleaf via "From External URL":

```https://bitbucket.org/ilikecheese8888_classes/latex/raw/master/{filepath}```

For example myCommands.sty would be:

```https://bitbucket.org/ilikecheese8888_classes/latex/raw/master/myCommands.sty```

##Packages:
*	[General Commands](https://bitbucket.org/ilikecheese8888_classes/latex/src/master/General/README.md)
*	[MATH Commands](https://bitbucket.org/ilikecheese8888_classes/latex/src/master/MATH/README.md)
*	[ECE Commands](https://bitbucket.org/ilikecheese8888_classes/latex/src/master/ECE/README.md)


##Command List
*	[\magnitude{}](#markdown-header-magnitude)

##Commands Overview
#####\magnitude{}
*	Places bars around the item in curly braces to show it as a magnitude, or absolute value
*	Bars will resize to cover the entire function held within \magnitude{}
*	Example:
	*	`\magnitude{I}` renders: `|I|`

	[Back to List](#markdown-header-command-list)