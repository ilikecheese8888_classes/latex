# ECE Commands
## Overleaf linking URL:
```https://bitbucket.org/ilikecheese8888_classes/latex/raw/master/MATH/statCommands.sty```

[Main README page](https://bitbucket.org/ilikecheese8888_classes/latex/src/master/README.md)


## Commands List
### STAT Commands

#### [General Commands](#markdown-header-general)
+	[\probability{}](#markdown-header-probability)

#### [Distribution Commands](#markdown-header-distributions)
+	[\distOf{}](#markdown-header-distof)
+	[\norm{}{}](#markdown-header-norm)
+	[\bin{}{}](#markdown-header-bin)
+	[\binPDF{}{}](#markdown-header-binpdf)
+	[\TIbinCDF{}{}[]](#markdown-header-TIbinCDF)
+	[\HPbinCDF{}{}{}[]](#markdown-header-HPbinCDF)
+	[\TIbinPDF{}{}[]](#markdown-header-TIbinPDF)
+	[\HPbinPDF{}{}{}](#markdown-header-HPbinPDF)
+	[\geo{}](#markdown-header-geo)
+	[\poisson{}](#markdown-header-poisson)
__________
## Commands Overview
### STAT Commands

#### General
__________
##### `\probability{}`
+	Outputs an expression of probability
+	Usage: `\probability{<value>}`
+	Results in: `P\parenthesis{<value>}`
+	Example:
	+	`\probability{X = x}` renders: `P(X=x)`
	
    [Back to List](#markdown-header-general-commands)


#### Distributions
__________
##### `\norm{}{}`
[comment]: #(+	A short form of suinit's \ampere)
[comment]: #(+	Usage: `\si{\ampere}`)
[comment]: #(+	Results in: `A`	)
[comment]: #(+	Example:)
[comment]: #(	+	`12 \si{\Amp}` renders: `12A`)
[comment]: #(	+	`12\ \si{\Amp}` renders: `12 A`)
[comment]: #(	)
[comment]: #(		[Back to List](#markdown-header-distribution-commands))